#######################################################################################
#                                                                                     #
#  EEEEE  PPPP   IIIII   CCCC  PPPP   L       AAA   Y   Y  L      IIIII   SSS  TTTTT  #
#  E      P   P    I    C      P   P  L      A   A   Y Y   L        I    S       T    #
#  E      PPPP     I    C      PPPP   L      A   A    Y    L        I    S       T    #
#  EEE    P        I    C      P      L      AAAAA    Y    L        I     SSS    T    #
#  E      P        I    C      P      L      A   A    Y    L        I        S   T    #
#  E      P        I    C      P      L      A   A    Y    L        I        S   T    #
#  EEEEE  P      IIIII   CCCC  P      LLLLL  A   A    Y    LLLLL  IIIII   SSS    T    #
#                                                                                     #
#######################################################################################

#GENENERALITES#
###############

* Le but de ce projet est de créé un site simple dans la lignée de MyCircle afin de mieux correspondre à nos attentes.
* L'objectif premier est une utilisation personnelle.
* Chaque participant sera identifié par un pseudo aléatoire qui serai personnalisable par un simple clic sur son pseudo (Option deux : pop up pour choisir un pseudo à la connexion au site).
* Chaque participant pourra ajouter une vidéo à la suite de la playlist à l'aide d'un lien youtube, dailymotion ou vimeo. 
* (Facultatif : chaque participant peut voter pour la musique s'il y 90% de vote négatif la vidéo est passée)

